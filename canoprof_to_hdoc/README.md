# Convertisseur Canoprof vers HDOC

## User story

Un utilisateur a cr�� un cours de math�matiques � l'aide de l'outil Canoprof. Il souhaite, pour une raison quelconque, convertir son projet (et donc ses activit�s et programmations) dans un autre format. Pour cela il utilise notre convertisseur qui permet pr�alablement une conversion vers un format interm�diare (hdoc). Il fait donc un clic droit sur l'�l�ment principal de son projet sous Canoprof et clique sur "exporter l'archive". Il laisse les options (Inclure le r�seau descendant complet des items s�lectionn�s, et Pr�server les espaces de l'atelier) par d�faut. Il d�place ensuite le fichier .scar obtenu dans le dossier input du convertisseur puis ex�cute le fichier run.bat (ou run.sh sous un syst�me UNIX). Une fois l'ex�cution termin�e, il r�cup�re son hdoc dans le dossier output.
