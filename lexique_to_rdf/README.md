Converter lexique_to_rdf

License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

Crédits
Estelle de Magondeaux
Marouane Hammi
Antoine Aufrechter

Install
In order to use this converter, follow those steps :
1. Copy your Lexique file(s) (.scar or .zip) into the input directory.
2. Execute the run that correspond to your OS.
3. You will find the result into the output directory. The name of the output files depends on the hour it was processed.

Dependance

User documentation

Unsupported

Known bugs

TODO
An onthology has to be created to fit the needs of this conversion.
A file which includes all the .rdf between rdf:RDF tag has to be created.
AVterm have to be handled

Capitalisation
RDF files are interesting because they can be used to create maps of datas. Here are any links to intersting applications using RDF.
http://www.visualdataweb.org/relfinder/relfinder.php
http://simile.mit.edu/welkin/
