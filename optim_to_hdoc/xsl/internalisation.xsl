<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:of="scpf.org:office"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns="http://www.utc.fr/ics/hdoc/xhtml">
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:param name="currentDir"/>
    
    <!-- Identity transformation -->
    <xsl:template match="node()|@*">
        <xsl:param name="src" />
        <xsl:copy>
            <xsl:apply-templates select="node()|@*">
                <xsl:with-param name="src" select="$src"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="src"><xsl:value-of select="$currentDir"/>/</xsl:variable>
        <xsl:copy>
            <xsl:apply-templates select="node()|@*">
                <xsl:with-param name="src" select="$src"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="sp:main[@sc:refUri]" priority="1">
        <xsl:param name="src" />
        <sp:main>
            <xsl:variable name="refUri" select="@sc:refUri" />
            <xsl:variable name="refDoc" select="document($refUri)" />
            <xsl:variable name="refUriWithoutLastPart">
                <xsl:call-template name="removeLastPart">
                    <xsl:with-param name="value" select="$refUri" />
                    <xsl:with-param name="everything" select="$refUri" />
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="totalUri">
                <xsl:value-of select="concat($src, $refUriWithoutLastPart)" />
            </xsl:variable>
            <xsl:apply-templates select="$refDoc/sc:item/of:section" >
                <xsl:with-param name="src" select="$totalUri"/>
            </xsl:apply-templates>
        </sp:main>
    </xsl:template>
    
    <xsl:template match="sp:sec[@sc:refUri]" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="refUriWithoutLastPart">
            <xsl:call-template name="removeLastPart">
                <xsl:with-param name="value" select="$refUri" />
                <xsl:with-param name="everything" select="$refUri" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUriWithoutLastPart)" />
        </xsl:variable>
        <xsl:apply-templates select="$refDoc/sc:item/of:section" >
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sp:subPage[@sc:refUri]" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="refUriWithoutLastPart">
            <xsl:call-template name="removeLastPart">
                <xsl:with-param name="value" select="$refUri" />
                <xsl:with-param name="everything" select="$refUri" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUriWithoutLastPart)" />
        </xsl:variable>
        <xsl:apply-templates select="$refDoc/sc:item/of:page | $refDoc/sc:item/of:section | $refDoc/sc:item/of:folder" >
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sp:page[@sc:refUri]" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="refUriWithoutLastPart">
            <xsl:call-template name="removeLastPart">
                <xsl:with-param name="value" select="$refUri" />
                <xsl:with-param name="everything" select="$refUri" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUriWithoutLastPart)" />
        </xsl:variable>
        <xsl:comment>Page in dir : <xsl:value-of select="$totalUri"/></xsl:comment>
        <xsl:apply-templates select="$refDoc/sc:item/of:page | $refDoc/sc:item/of:section | $refDoc/sc:item/of:folder" >
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    
    <xsl:template match="sp:frag" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUri)" />
        </xsl:variable>
        <xsl:apply-templates select="$refDoc/sc:item/of:fragment" >
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sc:uLink[@role='def']" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUri)" />
        </xsl:variable>
        <xsl:apply-templates select="$refDoc/sc:item/of:def">
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sc:uLink[@role='ref' and contains(@sc:refUri, '.section')]" priority="2">
        <xsl:param name="src" />
        <xsl:choose >
            <xsl:when test="../self::sc:para">
                <xsl:value-of select="." />
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="refUri" select="@sc:refUri" />
                <xsl:variable name="refDoc" select="document($refUri)" />
                <xsl:variable name="totalUri">
                    <xsl:value-of select="concat($src, $refUri)" />
                </xsl:variable>
                <xsl:apply-templates select="$refDoc/sc:item/of:section">
                    <xsl:with-param name="src" select="$totalUri"/>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="sc:uLink[@role='ref' and @sc:refUri]" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:comment>sc:uLink src = <xsl:value-of select="$src"/> refUri = <xsl:value-of select="$refUri"/></xsl:comment>
        <sc:uLink role="ref" sc:refUri="{$src}{$refUri}">
            <xsl:value-of select="."/>
        </sc:uLink>
    </xsl:template>

    <xsl:template match="sp:evtList[contains(@sc:refUri, '.object')]" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUri)" />
        </xsl:variable>
        <xsl:apply-templates select="$refDoc/sc:item/of:eventList">
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sp:tree[contains(@sc:refUri, '.tree')]" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUri)" />
        </xsl:variable>
        <xsl:apply-templates select="$refDoc/sc:item/of:tree">
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sc:extBlock[@role='img' and @sc:refUri]" priority="1">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <sc:extBlock role="img" sc:refUri="{$src}{$refUri}"></sc:extBlock>
    </xsl:template>
        
    <xsl:template match="sp:gallery[@sc:refUri]">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:variable name="refUriWithoutLastPart">
            <xsl:call-template name="removeLastPart">
                <xsl:with-param name="value" select="$refUri" />
                <xsl:with-param name="everything" select="$refUri" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="totalUri">
            <xsl:value-of select="concat($src, $refUriWithoutLastPart)" />
        </xsl:variable>
        <xsl:apply-templates select="$refDoc/sc:item/of:gallery" >
            <xsl:with-param name="src" select="$totalUri"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sp:img[@sc:refUri]">
        <xsl:param name="src" />
        <xsl:variable name="refUri" select="@sc:refUri" />
        <sp:img role="ref" sc:refUri="{$src}{$refUri}">
            <xsl:value-of select="."/>
        </sp:img>
    </xsl:template>
        
    <xsl:template name="removeLastPart">
        <xsl:param name="everything" />
        <xsl:param name="value" />
        <xsl:variable name="separator" select="'/'" />
        
        <xsl:choose>
            <xsl:when test="contains($value, $separator)">
                <xsl:call-template name="removeLastPart">
                    <xsl:with-param name="value" select="substring-after($value, $separator)" />
                    <xsl:with-param name="everything" select="$everything" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring-before($everything, $value)" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="of:folder">
        <xsl:param name="src" />
        <xsl:copy>
            <xsl:comment>Web Folder in dir : <xsl:value-of select="$src"/></xsl:comment>
            <xsl:apply-templates select="node()|@*">
                <xsl:with-param name="src" select="$src"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
    
    
    <!--Retracer le bon lien pour tous les refUri...-->
    
</xsl:stylesheet>
