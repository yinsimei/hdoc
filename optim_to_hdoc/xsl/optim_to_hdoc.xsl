<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:of="scpf.org:office"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns="http://www.utc.fr/ics/hdoc/xhtml">
    
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:processing-instruction name="oxygen">
            RNGSchema="http://scenari.utc.fr/hdoc/schemas/xhtml/hdoc1-xhtml.rng" type="xml"
        </xsl:processing-instruction>
        <html>
            <head>
                <title><xsl:value-of select="//sp:title[1]" /></title>
                <meta charset="utf-8"/>
                <xsl:variable name="author" select="//sp:copyright[1]" />
                <meta name="author" content="{$author}"/>
            </head>
            <body>
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>
    
    <xsl:template name="paper" match="of:paper">
        <xsl:apply-templates select="sp:pre | sp:chap | sp:post"/>
    </xsl:template>
    
    <xsl:template name="diaporama" match="of:slideshow">
        <xsl:apply-templates select="sp:slide | sp:part"/>
    </xsl:template>
    
    <xsl:template name="siteWeb" match="of:webSite">
        <xsl:apply-templates select="sp:margin | sp:home | sp:page | of:page | of:folder | of:section"/>
        <!--<xsl:apply-templates/>-->
    </xsl:template>
    
    <xsl:template match="sp:home | sp:page">
        <xsl:apply-templates select="of:page"/>
    </xsl:template>
    
    <xsl:template match="sp:slide | sp:part">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!--<xsl:template match="//*[@sc:refUri]">
        <xsl:variable name="refUri" select="@sc:refUri" />
        <xsl:variable name="refDoc" select="document($refUri)" />
        <xsl:choose >
            <xsl:when test="../self::sc:para">
                <xsl:apply-templates select="$refDoc/sc:item/of:def" />
                <xsl:apply-templates select="$refDoc/sc:item/of:page/sp:main" />
                <xsl:apply-templates select="$refDoc/sc:item/of:eventList" />
                <xsl:apply-templates select="$refDoc/sc:item/of:tree" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="$refDoc/sc:item/of:section" />
                <xsl:apply-templates select="$refDoc/sc:item/of:def" />
                <xsl:apply-templates select="$refDoc/sc:item/of:page/sp:main" />
                <xsl:apply-templates select="$refDoc/sc:item/of:eventList" />
                <xsl:apply-templates select="$refDoc/sc:item/of:tree" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>-->
    
    <xsl:template name="definition" match="of:def">
        <!--<xsl:choose>
            <xsl:when test="../self::sc:para">
                <em><xsl:value-of select="of:defM/sp:term"/></em> : <xsl:value-of select="sp:def/of:sTxt/sc:para"/>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <em><xsl:value-of select="of:defM/sp:term"/></em> : <xsl:value-of select="sp:def/of:sTxt/sc:para"/>
                </p>
            </xsl:otherwise>
        </xsl:choose>-->
        <em><xsl:value-of select="of:defM/sp:term"/></em> : <xsl:value-of select="sp:def/of:sTxt/sc:para"/>
    </xsl:template>
    
    <xsl:template name="section" match="of:section">
        <xsl:choose>
            <xsl:when test="of:section">
                <section>
                    <header>
                        <!--<h1><xsl:value-of select="of:sectionM/sp:title"/> </h1>-->
                        <xsl:choose>
                            <xsl:when test="string(of:sectionM/sp:title)">
                                <h1><xsl:value-of select="of:sectionM/sp:title"/>&#160;</h1>
                            </xsl:when>
                            <xsl:otherwise>
                                <h1>&#160;</h1>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:call-template name="author"></xsl:call-template>
                    </header>
                    <xsl:apply-templates select="sp:sec | sp:content | of:section"/>
                </section>
            </xsl:when>
            
            <xsl:otherwise>
                <section>
                    <xsl:attribute name="data-hdoc-type">unit-of-content</xsl:attribute>
                    <header>
                        <!--<h1><xsl:value-of select="of:sectionM/sp:title"/> </h1>-->
                        <xsl:choose>
                            <xsl:when test="string(of:sectionM/sp:title)">
                                <h1><xsl:value-of select="of:sectionM/sp:title"/>&#160;</h1>
                            </xsl:when>
                            <xsl:otherwise>
                                <h1>&#160;</h1>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:call-template name="author"></xsl:call-template>
                    </header>
                    <xsl:apply-templates select="sp:sec | sp:content | of:section"/>
                </section>
            </xsl:otherwise>
            
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="block" match="of:block">
        <div>
            <xsl:apply-templates select="of:blockM/sp:title"/>
            <xsl:apply-templates select="sp:co/of:flow/sp:txt/of:txt"/>
        </div>
    </xsl:template>
    
    <xsl:template name="block_emphasis" match="sp:emphasis/of:block">
        <div>
            <xsl:attribute name="data-hdoc-type">emphasis</xsl:attribute>
            <xsl:apply-templates select="of:blockM/sp:title"/>
            <xsl:apply-templates select="sp:co/of:flow/sp:txt/of:txt"/>
        </div>
    </xsl:template>
    
    <xsl:template name="block_complementaire" match="sp:extra/of:block">
        <div>
            <xsl:attribute name="data-hdoc-type">complement</xsl:attribute>
            <xsl:apply-templates select="of:blockM/sp:title"/>
            <xsl:apply-templates select="sp:co/of:flow/sp:txt/of:txt"/>
        </div>
    </xsl:template>
    
    <xsl:template name="para" match="sc:para">
        <p>
            <xsl:apply-templates />
        </p>
    </xsl:template>
    
    <xsl:template name="inline" match="sc:inlineStyle | sc:phrase | sc:inlineImg" >
       
            <xsl:choose>
                <xsl:when test="@role='emphasis' or @role='special'">
                    <em>
                        <xsl:value-of select="."/>
                    </em>
                </xsl:when>
                <xsl:when test="@role='quote'">
                    <q>
                        <xsl:value-of select="."/>
                    </q>
                </xsl:when>
                <xsl:when test="@role='url'">
                    <xsl:variable name="href" select="@url" />
                    <a href="{$href}">
                        <xsl:value-of select="."/>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        
    </xsl:template>
    
    <xsl:template match="sc:uLink">
        <xsl:choose>
            <xsl:when test="@role='url'">
                <xsl:variable name="href" select="@url" />
                <a href="{$href}">
                    <xsl:value-of select="."/>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="src" select="@sc:refUri" />
                <xsl:variable name="alt" select="." />
                <xsl:call-template name="parseUriImgHref">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="parseRef">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="alt" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <a href="optim/{$src}"><xsl:value-of select="$alt"/></a>
    </xsl:template>
    
    <xsl:template name="image" match="*[contains(@sc:refUri, '.jpg') or contains(@sc:refUri, '.png')  or contains(@sc:refUri, '.PNG')  or contains(@sc:refUri, '.gif') ]" >
        <xsl:variable name="src" select="@sc:refUri" />
        <xsl:variable name="alt" select="." />
        
        <xsl:choose >
            <xsl:when test="../self::sc:para">
                
                <xsl:call-template name="parseUriImgHref">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:when>
            <xsl:otherwise>
                
                <xsl:call-template name="parseUriImg">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:template name="audio" match="*[contains(@sc:refUri, '.mp3')]" >
        <xsl:variable name="src" select="@sc:refUri" />
        <xsl:variable name="alt" select="." />
        
        <xsl:choose >
            <xsl:when test="../self::sc:para">
                
                <xsl:call-template name="parseUriAudioHref">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:when>
            <xsl:otherwise>
                
                <xsl:call-template name="parseUriAudio">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="odg" match="*[contains(@sc:refUri, '.odg')]" >
        <xsl:variable name="src" select="@sc:refUri" />
        <xsl:variable name="alt" select="." />
        
        <xsl:choose >
            <xsl:when test="../self::sc:para">
                
                <xsl:call-template name="parseUriOdgHref">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:when>
            <xsl:otherwise>
                
                <xsl:call-template name="parseUriOdg">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="ods" match="*[contains(@sc:refUri, '.ods')]" >
        <xsl:variable name="src" select="@sc:refUri" />
        <xsl:variable name="alt" select="." />
        
        <xsl:choose >
            <xsl:when test="../self::sc:para">
                
                <xsl:call-template name="parseUriOdsHref">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:when>
            <xsl:otherwise>
                
                <xsl:call-template name="parseUriOds">
                    <xsl:with-param name="src" select="$src"/>
                    <xsl:with-param name="tmp" select="$src"/>
                    <xsl:with-param name="alt" select="$alt"/>
                </xsl:call-template>
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="itemizedList" match="sc:itemizedList">
        <ul>
            <xsl:for-each select="sc:listItem">
                <li>
                    <p>
                        <xsl:value-of select="." />
                    </p>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    
    <xsl:template name="orderedList" match="sc:orderedList">
        <ol>
            <xsl:for-each select="sc:listItem">
                <li>
                    <p>
                        <xsl:value-of select="." />
                    </p>
                </li>
            </xsl:for-each>
        </ol>
    </xsl:template>
    
    <xsl:template name="table" match="sc:table">
        <table>
            <xsl:for-each select="sc:row">
                <tr>
                    <xsl:for-each select="sc:cell">
                        <td>
                            <p>
                                <xsl:value-of select="." />
                            </p>
                        </td>
                    </xsl:for-each>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template name="parseUriImg">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="alt" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<img src="{$src}/{$fileName}" alt="{$alt}"/>-->
        <img src="{$src}/{$fileName}" alt="{$alt}"/>
    </xsl:template>
    
    <xsl:template name="parseUriImgHref">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="alt" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<a href="optim/{$src}"><xsl:value-of select="$alt"/></a>-->
        <a href="{$src}/{$fileName}"><xsl:value-of select="$alt"/></a>
    </xsl:template>
    
    <xsl:template name="parseUriAudio">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<audio src="optim/{$src}" />-->
        <audio src="{$src}/{$fileName}" />
    </xsl:template>
    
    <xsl:template name="parseUriAudioHref">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="alt" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<a href="optim/{$src}" ><xsl:value-of select="$alt"/></a>-->
        <a href="{$src}/{$fileName}" ><xsl:value-of select="$alt"/></a>
    </xsl:template>
    
    <xsl:template name="parseUriOdg">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<object type="application/vnd.oasis.opendocument.graphics " data="optim/{$src}"/>-->
        <object type="application/vnd.oasis.opendocument.graphics " data="{$src}/{$fileName}"/>
    </xsl:template>
    
    <xsl:template name="parseUriOdgHref">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="alt" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<a href="optim/{$src}"><xsl:value-of select="$alt"/></a>-->
        <a href="{$src}/{$fileName}"><xsl:value-of select="$alt"/></a>
    </xsl:template>
    
    <xsl:template name="parseUriOds">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<object type="application/vnd.oasis.opendocument.spreadsheet" data="optim/{$src}"/>-->
        <object type="application/vnd.oasis.opendocument.spreadsheet" data="{$src}/{$fileName}"/>
    </xsl:template>
    
    <xsl:template name="parseUriOdsHref">
        <xsl:param name="src" />
        <xsl:param name="tmp" />
        <xsl:param name="alt" />
        <xsl:param name="fileName">
            <xsl:call-template name="getLastPart">
                <xsl:with-param name="value" select="$src" />
            </xsl:call-template>
        </xsl:param>
        <!--<a href="optim/{$src}"><xsl:value-of select="$alt"/></a>-->
        <a href="{$src}/{$fileName}"><xsl:value-of select="$alt"/></a>
    </xsl:template>
    
    <xsl:template name="eventList" match="of:eventList">
        <!--<div>
            <xsl:apply-templates select="of:eventListM/sp:title"/>
        </div>-->
            <xsl:apply-templates select="sp:event"/>
    </xsl:template>
    
    <xsl:template name="event" match="sp:event">
        <div>
            <xsl:apply-templates select="of:event/of:eventM/sp:title"/>
            <p><xsl:value-of select="of:event/of:eventM/sp:date"/></p>
            <xsl:apply-templates select="of:event/sp:co/of:flow/sp:txt/of:txt/sc:para"/>
        </div>
    </xsl:template>
    
    <xsl:template name="tree" match="of:tree">
        <div>
            <xsl:apply-templates select="of:treeM/sp:title"/>
            <xsl:apply-templates select="of:treeM/sp:ico"/>
            <p><xsl:value-of select="of:treeM/sp:shortDescription"/></p>
            <xsl:apply-templates select="sp:description/of:flow/sp:txt/of:txt/sc:para"/>
        </div>
        <xsl:apply-templates select="sp:node/of:tree"/>
    </xsl:template>
    
    <xsl:template name="filter" match="of:filter">
    </xsl:template>
    
    <xsl:template name="webpage" match="of:page">
        <section>
            <header>
                <!--<h1><xsl:value-of select="of:pageM/sp:title"/></h1>-->
                <xsl:choose>
                    <xsl:when test="string(of:pageM/sp:title)">
                        <h1><xsl:value-of select="of:pageM/sp:title"/>&#160;</h1>
                    </xsl:when>
                    <xsl:otherwise>
                        <h1>&#160;</h1>
                    </xsl:otherwise>
                </xsl:choose>
            </header>
            <xsl:apply-templates select="sp:main | sp:margin | of:page | sp:subPage | of:section"/>
        </section>
    </xsl:template>
    
    <xsl:template match="sp:main">
        <xsl:apply-templates select="sp:sec | sp:content | of:section | sp:margin"/>
    </xsl:template>
    
    <xsl:template match="sp:title">
        <xsl:choose>
            <xsl:when test="string(.)">
                <h6><xsl:value-of select="."/>&#160;</h6>
            </xsl:when>
            <xsl:otherwise>
                <h6>&#160;</h6>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="sp:margin">
        <section>
            <header>
                <h1>En Marge</h1>
            </header>
            <xsl:apply-templates/>
        </section>
    </xsl:template>
    
    <xsl:template name="getLastPart">
        <xsl:param name="value" />
        <xsl:variable name="separator" select="'/'" />
        
        <xsl:choose>
            <xsl:when test="contains($value, $separator)">
                <xsl:call-template name="getLastPart">
                    <xsl:with-param name="value" select="substring-after($value, $separator)" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$value" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="author">
        <xsl:if test="//of:paperM/sp:commonMeta/of:commonM/sp:copyright">
            <div>
                <xsl:attribute name="data-hdoc-type">author</xsl:attribute>
                <xsl:value-of select="//of:paperM/sp:commonMeta/of:commonM/sp:copyright"/>
            </div>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="of:folder">
        <section>
            <header>
                <xsl:choose>
                    <xsl:when test="string(of:folderM/sp:title)">
                        <h1><xsl:value-of select="of:folderM/sp:title"/></h1>
                    </xsl:when>
                    <xsl:otherwise>
                        <h1>&#160;</h1>
                    </xsl:otherwise>
                </xsl:choose>
            </header>
            <xsl:apply-templates select="sp:subPage | sp:page | of:section"/>
        </section>
    </xsl:template>
    
    <xsl:template match="of:gallery">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
</xsl:stylesheet>
