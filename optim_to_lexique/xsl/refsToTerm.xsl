<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:lx="scpf.org:lexicon"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:of="scpf.org:office"
    
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml" indent="yes" />
    
    <xsl:template match="/">
        <sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
            <lx:term xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:lx="scpf.org:lexicon">
                <lx:termM>
                    <sp:name><xsl:value-of select="sc:item/of:def/of:defM/sp:term"></xsl:value-of></sp:name>                    
                </lx:termM>
                <sp:def>
                    <lx:definition>
                        <sp:desc>
                            <lx:defTxt>
                                <xsl:apply-templates select="sc:item/of:def/sp:def/of:sTxt/*"></xsl:apply-templates>
                            </lx:defTxt>
                        </sp:desc>
                    </lx:definition>
                </sp:def>
            </lx:term>
        </sc:item>
    </xsl:template>
    
    <xsl:template match="of:sTxt//*|of:sTxt//@*" >
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
    
    
    <!-- filtrage du contenu -->
    <xsl:template match="sc:uLink[@role='url']" priority="2">
        <sc:phrase>
            <xsl:attribute name="role">url</xsl:attribute>
            <lx:urlM>
                <sp:url><xsl:value-of select="@url"></xsl:value-of></sp:url>
                <sp:title><xsl:value-of select="text()"></xsl:value-of></sp:title>
            </lx:urlM>
            <xsl:value-of select="text()"></xsl:value-of>
        </sc:phrase>
    </xsl:template>    
</xsl:stylesheet>