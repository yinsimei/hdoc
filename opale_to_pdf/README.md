﻿Converter opale_to_pdf
-----------------------

* WARNING : This module is not functionnal yet !

The purpose of this converter is to obtain a PDF file from an Opale document.

The Hdoc pivot format is used to convert one format to another, losing the least amount of information. It is based on HTML.


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Thibault DRAIN
* Christophe VIROT
* Pierre Lemaire
* Baptiste PERRAUD
* Raphaël DEBRAY


Dependance
----------

This converter is based on two converters :
* Opale to Hdoc
* Hdoc to Pdf


User documentation
------------------


Unsupported 
-----------

Refer to the unsupported elements in Opale to Hdoc and in Hdoc to Pdf. 


Known bugs
----------

Refer to the known bugs in Opale to Hdoc and in Hdoc to Pdf. 


Todo
---- 


Technical notes
---------------

The converter contains 1 file:

* opale_to_pdf.ant
        
    It checks wether or not the user specified the parameters and performs the following : 
    * Copy the input file in the opale_to_hdoc directory.
    * Perform the opale_to_hdoc transformation.
    * Copy the result in the hdoc_to_pdf directory. 
    * Perform the hdoc_to_pdf transformation. 
    * Copy the result in the opale_to_pdf directory. 


Capitalisation
--------------

N/A
