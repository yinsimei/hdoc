# Hdoc to Mongo (`hdoc_to_mongo`)

## License
License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
Alexandre Thouvenin
Kapilraj Thangeswaran

## Presentation
This module is able to extract data from a file in Hdoc format and insert them into MongoDB.

## Dependencies
No dependance needed.

## User stories
En tant qu’utilisateur rédigeant des documents sous opale, je veux récupérer les exercices corrigés d’un sujet donné dans le but de les réutiliser.
En tant qu’utilisateur rédigeant des documents sous opale, je veux récupérer les cours d’un auteur précis dans le but d’utiliser ses cours comme références.
En tant qu’utilisateur universitaire, je souhaite mettre à disposition mes exercices sur internet de manière structuré dans le but de permettre à d’autre utilisateurs universitaires de les trouver, par thème, auteur ou contenu et de les réutiliser.
