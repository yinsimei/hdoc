<?xml version="1.0" encoding="UTF-8"?>
	<?oxygen RNGSchema="http://hdoc.crzt.fr/schemas/xhtml/hdoc1-xhtml.rng" type="xml"
        ?><html xmlns="http://www.utc.fr/ics/hdoc/xhtml" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:op="utc.fr:ics/opale3"><head><title>XSLT (opérations avancées) : expressions régulières et fichiers texte</title><meta charset="utf-8"/><meta content="HdocConverter/Opale3.4" name="generator"/><meta name="keywords" content="XSL"/><meta name="keywords" content="XSLT"/><meta name="keywords" content="XSL-XSLT"/><meta name="keywords" content="Expression régulière"/><meta name="rights" content="by-sa"/><meta name="author" content="Karim El Aktaa (Contributions : Stéphane Crozat et les étudiants de NF29)"/></head><body><section><header><h1>Objectifs</h1></header><div><ul><li><p>Utiliser un fichier de texte brut non XML</p></li><li><p>Utiliser des expressions régulières</p></li></ul></div></section><section data-hdoc-type="introduction"><header><h1>Introduction</h1></header><div><p>XSL-XSLT est un langage de programmation qui permet notamment de manipuler des documents XML.</p><p>Ce module a pour but de vous apporter des connaissances plus approfondies sur ce langage que vous connaissez déjà : l'utilisation de document non XML et d'expressions régulières.</p></div></section><section><header><h1>Cours</h1></header><section><header><h1>Lecture de fichier texte non XML</h1></header><div><h6>Introduction</h6><p>Dans cette partie du cours, nous allons voir comment utiliser un fichier texte brut (non XML) avec le langage XSLT.</p></div><div data-hdoc-type="complement"><h6>Limitation du XSLT</h6><p>Dans son principe de fonctionnement originel, XSLT est conçu pour prendre en entrée un fichier XML bien formé.</p></div><div data-hdoc-type="emphasis"><h6>Lecture de fichier non XML</h6><p>XSLT 2.0 introduit la possibilité de lire des fichiers texte qui ne sont pas nécessairement des fichiers XML bien formés.</p><p>La fonction :</p><p>unparsed-text($href as xs:string?, $encoding as xs:string) as xs:string?</p><p>permet en effet de lire une ressource externe et de renvoyer le résultat sous forme d'une chaîne de caractères.</p><p>L'argument <em>$href</em> correspond à l'URI de la ressource externe à lire.</p><p>Il est également possible de préciser un encodage spécifique via l'argument optionnel <em>$encoding</em>.</p></div><div data-hdoc-type="example"><h6>Exemple de lecture d'un fichier texte non XML</h6><p>Lecture du fichier <em>txt.txt</em>, en utilisant l'encoding <em>iso-8859-1</em>. La fonction <em>unparsed-text(...)</em> récupère le contenu du fichier et renvoie le résultat sous forme d'une chaîne de caractère à la commande <em>xsl :value-of</em>, qui affiche donc le contenu du fichier lu.</p><p>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
      xmlns:xs="http://www.w3.org/2001/XMLSchema"
      exclude-result-prefixes="xs"
      version="2.0"&gt;
    
&lt;xsl:output method="text"&gt;&lt;/xsl:output&gt;
    
    &lt;xsl:template match="/"&gt;
        &lt;xsl:value-of select="unparsed-text('txt.txt', 'iso-8859-1')" disable-output-escaping="yes"/&gt;
    &lt;/xsl:template&gt;
    
&lt;/xsl:stylesheet&gt;</p><p>Le paramètre <em>disable-output-escaping</em>, permet d'indiquer si les caractères spéciaux (comme '&lt;' par exemple) sont échappés ou non.</p></div><div data-hdoc-type="advice"><h6>Comment lancer la transformation sans fichier XML source ?</h6><p>Habituellement l'utilisation d'une transformation XSLT se fait via le document XML source (en utilisant la <i>processing instruction</i> <q>&lt;?xml-stylesheet ... ?&gt;</q>).</p><p>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;?xml-stylesheet type="xsl" href="poeme.xsl"?&gt;

&lt;poeme titre="The Stone Troll" auteur="JRR Tolkien"&gt;
&lt;strophe&gt;
&lt;vers&gt;Troll sat alone on his seat of stone,&lt;/vers&gt;
&lt;vers&gt;And munched and mumbled a bare old bone;&lt;/vers&gt;
&lt;/strophe&gt;
&lt;strophe&gt;
&lt;vers&gt;Up came Tom with his big boots on.&lt;/vers&gt;
&lt;vers&gt;Said he to Troll: 'Pray, what is yon?&lt;/vers&gt;
&lt;/strophe&gt;
&lt;/poeme&gt;</p><p>Pour pallier ce principe, si on ne souhaite pas faire appel à un fichier XML source, il faut utiliser le fichier XSLT (qui est du XML) comme entrée.</p></div><footer/></section><section><header><h1>Expression régulière avec XSL-XSLT</h1></header><div><h6>Introduction</h6><p>Dans cette partie du cours, nous allons voir comment utiliser des expressions régulières avec le langage XSLT, après avoir fait un rappel sur ce que sont les expressions régulières.</p></div><div data-hdoc-type="emphasis"><h6>Qu'est ce qu'une expression régulière ?</h6><p><q>Une expression régulière est une chaîne de caractères que l'on appelle parfois un motif et qui décrit un ensemble de chaînes de caractères possibles selon une syntaxe précise.</q></p><p>Une expression régulière est une suite de caractères typographiques (un « motif » ou « pattern ») décrivant une chaîne de caractères dans le but de la trouver dans un bloc de texte pour lui appliquer un traitement automatisé, comme un ajout, son remplacement ou sa suppression.</p><p>Syntaxe des expressions régulières dans XSL-XSLT/XPath.</p></div><div data-hdoc-type="example"><h6>Exemples d'expressions régulières</h6><table><caption>Exemples de regex</caption><tr><td><p>Expression régulière</p></td><td><p>Résultats</p></td></tr><tr><td><p>a</p></td><td><p>match le caractère a seul</p></td></tr><tr><td><p>a+b</p></td><td><p>match les mots commençants par a suivi de de zéro ou plusieurs a et finissant par b</p></td></tr><tr><td><p>[a-z]{5}</p></td><td><p>match les mots composés de 5 caractères compris entre a et z</p></td></tr><tr><td><p>a\.b+c?</p></td><td><p>match : a.b, a.bc, a.bb, a.bbc, a.bbbc...</p></td></tr><tr><td><p>(http|https|ftp):\/\/([a-zA-Z0-9]*)\.([a-zA-Z0-9]*)\.(com|net)</p></td><td><p>Pattern simpliste d'adresse web</p></td></tr></table></div><div data-hdoc-type="advice"><h6>Expressions régulières avec oXygen</h6><p>Outils -&gt; Constructeur des expressions régulières</p></div><div data-hdoc-type="emphasis"><h6>Les expressions régulières dans XSL-XSLT</h6><p>Dans le langage XSL-XSLT il y a 4 fonctions qui permettent d'utiliser les expressions régulières :</p><ol><li><p><em>tokenize</em></p></li><li><p><em>matches</em></p></li><li><p><em>replace</em></p></li><li><p><em>xsl:analyze-string</em></p></li></ol></div><div data-hdoc-type="emphasis"><h6>Tokenize</h6><p>La fonction <em>tokenize($input, $pattern)</em> est pratique car elle permet de séparer une chaîne de caractères en une séquence de chaînes de caractères.</p><p>L'argument<em> $input</em> désigne la chaîne de caractère d'entrée, et l'argument <em>$pattern</em> désigne le pattern (ou expression régulière) qui match le délimiteur choisi.</p></div><div data-hdoc-type="example"><h6>Exemple de récupération d'un fichier texte et de tokenize</h6><p>On cherche à lire un fichier texte et à remplacer les sauts de lignes par des points virgules.</p><p>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"&gt;
    
    &lt;xsl:output method="text"&gt;&lt;/xsl:output&gt;
    
    &lt;xsl:template match="/"&gt;
        &lt;xsl:for-each select="tokenize(unparsed-text('txt.txt', 'iso-8859-1'), '\r?\n')"&gt;
            &lt;xsl:copy&gt;&lt;/xsl:copy&gt;&lt;xsl:text&gt;;&lt;/xsl:text&gt;
        &lt;/xsl:for-each&gt;
    &lt;/xsl:template&gt;
    
&lt;/xsl:stylesheet&gt;
</p></div><div data-hdoc-type="emphasis"><h6>Matches</h6><p>La fonction <em>matches</em><em>($input, $pattern)</em> permet de vérifier si <em>$input</em> match l'expression régulière <em>$pattern</em>.</p></div><div data-hdoc-type="emphasis"><h6>Replace</h6><p>La fonction <em>replace</em><em>($input, $pattern, $replacement)</em> permet de remplacer les parties de <em>$input</em> qui matchent <em>$pattern</em> avec <em>$replacement</em>.</p></div><div data-hdoc-type="emphasis"><h6>Analyse de chaîne de caractère</h6><p>L'instruction <em>xsl:analyze-string</em> permet d'analyser une chaîne de caractère en fonction d'une expression régulière. On peut accéder aux sous chaînes de caractères qui correspondent à l'expression régulière avec l'instruction <em>xsl:matching-substring</em>, et inversement aux sous chaînes de caractères qui ne correspondent pas à la regex avec <em>xsl:non-matching-substring</em>.</p><p>&lt;xsl:analyze-string
  select = expression
  regex = { string }
  flags? = { string }&gt;
  &lt;!-- Content: (xsl:matching-substring?, xsl:non-matching-substring?, xsl:fallback*) --&gt;
&lt;/xsl:analyze-string&gt;</p></div><div data-hdoc-type="example"><h6>Exemple d'analyse de string avec une regex</h6><p>Cet exemple remplace tout les caractères '\n' (newline) par la balise &lt;br/&gt;.</p><p>&lt;xsl:analyze-string select="abstract" regex="\n"&gt;
  &lt;xsl:matching-substring&gt;
    &lt;br/&gt;
  &lt;/xsl:matching-substring&gt;
  &lt;xsl:non-matching-substring&gt;
    &lt;xsl:value-of select="."/&gt;
  &lt;/xsl:non-matching-substring&gt;
&lt;/xsl:analyze-string&gt;
</p></div><footer/></section><footer/></section><section><header><h1>Exercices</h1></header><footer/></section></body></html>
