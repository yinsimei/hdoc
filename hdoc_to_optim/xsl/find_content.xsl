﻿<?xml version="1.0" encoding="UTF-8"?>
<!--find_content.xsl creates a ANT file get_content-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:h="urn:utc.fr:ics:hdoc:container"
    xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <!--<xsl:param name="destfile" required="yes" as="xs:string"/>-->
	<xsl:param name="DocumentType" required="yes" as="xs:string"/>
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="h:container">
        <project name="hdoc_to_optim" default="main">
            <target name="main">
                <mkdir dir="result"/>
                <chmod dir="result" perm="777"/>
                <copy file="output/.wspmeta" todir="result"/>
                <xsl:apply-templates/>
            </target>
        </project>
    </xsl:template>
    
    <xsl:template match="h:rootfiles">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="h:rootfile">
        <xsl:variable name="path" select="@full-path"/>
        <!--the ANT runs the XSLT get_ressources then the ANT created by it-->
        <xslt in="hdoc/{$path}" out="get_ressources.ant" style="xsl/find_ressources.xsl"/>
        <chmod file="get_ressources.ant" perm="777"/>
        <ant antfile="get_ressources.ant"/>
        <!--the ANT runs the main XSLT transfo-->
		<xsl:choose>
			<xsl:when test="$DocumentType='paper'">
				<xslt in="hdoc/{$path}" out="result/content.paper" style="xsl/transfo.xsl" classpath="lib/saxon9he.jar">
					<factory name="net.sf.saxon.TransformerFactoryImpl"/>
				</xslt>
			</xsl:when>
			<xsl:when test="$DocumentType='slideshow'">
				<xslt in="hdoc/{$path}" out="result/content.slideshow" style="xsl/transfo2.xsl" classpath="lib/saxon9he.jar">
					<factory name="net.sf.saxon.TransformerFactoryImpl"/>
				</xslt>
			</xsl:when>
			<xsl:when test="$DocumentType='website'">
				<xslt in="hdoc/{$path}" out="result/content.website" style="xsl/transfo3.xsl" classpath="lib/saxon9he.jar">
					<factory name="net.sf.saxon.TransformerFactoryImpl"/>
				</xslt>
			</xsl:when>
			<xsl:when test="$DocumentType='webpage'">
				<xslt in="hdoc/{$path}" out="result/content.webpage" style="xsl/transfo4.xsl" classpath="lib/saxon9he.jar">
					<factory name="net.sf.saxon.TransformerFactoryImpl"/>
				</xslt>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message> Wrong document type </xsl:message>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>
</xsl:stylesheet>
