@echo off
set lib=lib
set ant=mindmapping_to_opale.ant
set antparam=-Dprogram.param=%1
set inputPath=%2

set scJarList=%lib%\*

if defined %inputPath% java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam% -DinputPath %inputPath%
pause
if not defined %inputPath% java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%
pause

REM start /MIN java.exe -classpath "%scJarList%" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% %antparam%
