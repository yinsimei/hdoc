﻿Converter hdoc_to_basex
-----------------------

The purpose of this converter is to obtain an XML data file suitable for importation into basex for futher XQuery requests from a HDOC file

License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Simei YIN
* Baptiste MONTANGE


Dependance
----------

This project can be used alone if you want to import an HDOC file into basex.


## User stories
------------------
- Among a group of courses, user can search by title, author or keywords of the course.
- By searching a certain keyword, user can obtain the sections that contain it with their hierarchy levels in the course.
- In a certain section, by searching a key word, user can obtain paragraphes that contain it.
- User can get definitions related to a keyword
- User can get examples whose titles contain a keyword