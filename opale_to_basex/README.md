﻿Converter opale_to_basex
-----------------------

The purpose of this converter is to obtain an HDOC file from an Opale document. 


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Simei YIN
* Baptiste MONTANGE


Dependance
----------

In order to work properly, this module needs

1. [`opale_to_hdoc`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/opale_to_hdoc) (Opale to Hdoc conversion)
2. [`hdoc_to_basex`](https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex) (Hdoc to Basex conversion)


User Stories
----------

Please consult the section "User Stories" of README.md file in [`hdoc_to_basex`] (https://gitlab.utc.fr/crozatst/hdoc/tree/master/hdoc_to_basex/README.md)