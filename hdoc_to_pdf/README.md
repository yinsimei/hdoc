﻿Converter hdoc_to_pdf
-----------------------

The purpose of this converter is to obtain an PDF file from a hdoc document. 


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Raphaël Debray

* Baptiste Perraud


Dependance
----------


This project can be used alone if you only want to convert a hdoc file into a PDF file.


User documentation
------------------




Known bugs
----------



Generic Todo
---- 

* Generate a clean PDF file (using the LaTeX formatting example)
    - Create a default CSS file with basic spine rules
    - Get the right free font (equivalent to the LaTeX's one)
* Generate the ToC according to the converted (by XSL) headings of the hdoc
* Handle as fully as possible of widows and girl orphans; trying to match Prince's layout and implementing the suitable CSS rules (which shall not be interpreted by FS)
* Allow the user to override some specific CSS rules, according to the main layout logical rules
* Bonus: find out a HTML editor to manually add line breaks to a hdoc file in order to resolve widows and girl orphans problems after the PDF file's generation


Technical notes
---------------

* The backlog of the current project is at the following url: <https://framemo.org/NF29_Opale_to_Pdf>
* The user stories of the current project are at the following url: <https://bimestriel.framapad.org/p/NF29_Opale_to_Pdf_userstory>



Capitalisation
--------------


