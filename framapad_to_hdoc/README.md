# Converter etherpad_to_hdoc

## License
License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

## Credits
- 2015
    - Jean-Côme Douteau
    - Gabrielle Rit
    - Jean Vintache
- 2014
    - Fecherolle Cécile

## Presentation
This module is able to convert several [etherpad](http://etherpad.org/) files (exported as html files) to the hdoc format.

## User documentation

## Running etherpad_to_hdoc.ant
1. Create an etherpad document and export it as an html file.
2. please place your html files in the `/input` folder
3. run the `run.[bat|sh]` script of your choice depending on your OS
4. and retrieve the hdoc outputs in the `/output` folder

## Unsupported
- Markdown
- Author paternity
- Etherpad timeline
- Chat

## Known bugs
- Nested lists in lists are not supported
Example : 
`<ul>
	<li>
		<ul>
			<li>
			Never gonna give you up.
			</li>
		</ul>
	</li>
<ul>`
- As a consequence, etherpad indentation is not supported because it is coded as nested lists.

## TODO
- Markdown

## Technical notes
### Description of etherpad_to_hdoc.ant

#### Prelude
- Importation of necessary classes (antlib, htmlcleaner, jing)
- Creation of directories architecture tree

#### Transformations
- Use of htmlcleaner to transform the input file from html to xhtml. For more info, see http://htmlcleaner.sourceforge.net/index.php.
- Apply html2xhtml.xsl : this xsl extracts the content into <body> tags
- Apply html2xhtmlv1.xsl : this xsl is used as a fix and adds br tag at the end of lists (ul and ol)
- Apply html2xhtmlv2.xsl : this xsl surround text line with p tags and transforms non-hdoc tags into hdoc tags as s, u, strong tags.
- Apply html2xhtml3.xsl : this xsl is used as a fix, it deletes p tags when its child is ul or ol
- Apply xhtml2hdoc.xsl : this xsl transforms the content into hdoc structure

#### Post-transformations actions
- Build hdoc structure
- Jing checks if the output file is validated with the right rng schema
- Zip the directory into hdoc archive

### Supported tags
- html tags -> hdoc tags
- u, s, em, strong -> em
- li -> li
- ol -> ol
- br -> p

## Capitalisation
Using regular expression with xsl is a good way to parse a non xml file.