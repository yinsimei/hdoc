<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <xsl:apply-templates select="//meta" mode="inHead"/>
                <title><xsl:value-of select="//title"/></title>
            </head>
            <body>
                <section>
                <xsl:apply-templates select="@*|node()[not(self::meta)]" />
                </section>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="title">
    </xsl:template>
    
    <xsl:template match="meta" mode="inHead">
        <xsl:for-each select="./*">
            <meta name="{local-name(.)}" content="{./text()}"/>
        </xsl:for-each>
        <meta charset="utf-8"/>
        <meta generator="HdocConverter/Etherpad"/>
    </xsl:template>
    
    <xsl:template match="meta">
    </xsl:template>
    
    <xsl:template match="h1 | h2 | h3 | h4 | h5 | h6">
        <xsl:copy>
            <xsl:value-of select="."/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="important">
        <div class="emphasis"><xsl:value-of select="."/></div>
    </xsl:template>
    
    <xsl:template match="exemple">
        <div class="example"><xsl:value-of select="."/></div>
    </xsl:template>
    
    <xsl:template match="definition">
        <div class="definition"><xsl:value-of select="."/></div>
    </xsl:template>
    
    <xsl:template match="br">
        <xsl:copy>
            <xsl:value-of select="."/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="br|b|i|ul|ol|li|u|del">
        <xsl:copy>
            <xsl:value-of select="."/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
