<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns="http://www.utc.fr/ics/hdoc/xhtml" version="2.0">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="p[child::ul or child::ol]" priority="1">

      <xsl:apply-templates select="./*"></xsl:apply-templates>

    </xsl:template>
    
    <!--Identity template, 
        provides default behavior that copies all content into the output -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
     
</xsl:stylesheet>