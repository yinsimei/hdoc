<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:h="http://www.utc.fr/ics/hdoc/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:cp="canope.fr:canoprof"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs">
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:param name="filename"/>

    <xsl:strip-space elements="*"/>

    <!-- This template matches the root. One hdoc file = one Canoprof's Programme" -->
    <xsl:template match="h:html">
        <sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
            <cp:program xmlns:cp="canope.fr:canoprof"
                xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
                <xsl:apply-templates select="./*"/>
            </cp:program>
        </sc:item>
    </xsl:template>

    <!-- Head related templates. -->
    <xsl:template match="h:head">
        <cp:programM>
            <!-- Title of Programme. -->
            <sp:title>
                <xsl:value-of select="./h:title"/>
            </sp:title>

            <!-- Hdoc's "date" = Programme's period. -->
            <xsl:apply-templates select="./h:meta[@name='date']"/>

            <!-- Hdoc's "description" = Programme's resume. -->
            <xsl:apply-templates select="./h:meta[@name='description']"/>

        </cp:programM>
    </xsl:template>

    <xsl:template match="h:head/h:meta[@name='date']">
        <sp:period>
            <xsl:value-of select="./@content"/>
        </sp:period>
    </xsl:template>

    <xsl:template match="h:head/h:meta[@name='description']">
        <sp:abstract>
            <cp:txtDesc>
                <sc:para xml:space="preserve">
                    <xsl:value-of select="./@content"/>
                </sc:para>
            </cp:txtDesc>
        </sp:abstract>
    </xsl:template>
    
    <!-- Body related templates. -->
    <xsl:template match="h:body">
        <xsl:if test="./*">
            <xsl:apply-templates select="./*"/>
        </xsl:if>
    </xsl:template>
    
    <!-- Section related templates -->
    <!-- CanoProf's Seance = hdoc's  body/Section = Opale's Division -->
    <!-- CanoProf's Activite TetM = hdoc's  body/Section/Section = Opale's Grain -->
    <!-- CanoProf's Activite TetM (section) = hdoc's  body/Section/Section/(Section...) = Opale's Grain (partie) -->
    
    <!-- TODO :CanoProf's Seance = hdoc's  body/Section = Opale's Division -->
    <xsl:template match="h:body/h:section">

    </xsl:template>
    
    <!-- TODO : if Section have only a dev (no sub section), we create a short activty to print text into a seance -->
    <xsl:template match="h:body/h:section/h:div">

    </xsl:template>
    
    <!-- TODO : CanoProf's Activite TetM = hdoc's  body/Section/Section = Opale's Grain -->
    <xsl:template match="h:body/h:section/h:section">

    </xsl:template>

</xsl:stylesheet>
