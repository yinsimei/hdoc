Hdoc to Canoprof
===

License
-------
This project is under [GPL 3.0 license](http://www.gnu.org/licenses/gpl-3.0.txt).

Credits
-------
### Autumn 2016

* Villain Benoit
* Luszcz Charlotte

Presentation
---
"Hdoc to Canoprof" is an hdoc converter to Canoprof files. It's a set of ANT scripts and XSL files.

Dependencies
---
There's no particular dependencies needed to run the converter.

User Story
---
### Running the script

* Put the `.hdoc` files in the input folder
* Run `run.bat` or `run.sh` according to your operating system
* The output files are in the output folder
