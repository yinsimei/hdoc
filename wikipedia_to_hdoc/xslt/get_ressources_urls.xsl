<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:saxon="http://saxon.sf.net/" 
    extension-element-prefixes="saxon"
    >
    
      <!-- This XSL create an ANT script that will download our ressources (images) using the GET TASK-->
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <project name="getRessourcesFiles" basedir="." default="getRessourcesFiles">
         
            <target name="getRessourcesFiles">
                <xsl:apply-templates select="//img"/>
            </target>
        </project>
    </xsl:template>
    
    
    <xsl:template match="img" priority="5">
        <xsl:param name="dest-rep">${hdocResultPath}/ressources</xsl:param>
        <xsl:param name="dest-file">${hdocResultPath}/ressources/<xsl:value-of select="translate(tokenize(@src, '/')[last()],'?%','_')"/></xsl:param>
         <mkdir dir="{$dest-rep}"/>
        <get src="{@src}" dest="{$dest-file}" />
    </xsl:template>
    

    
</xsl:stylesheet>