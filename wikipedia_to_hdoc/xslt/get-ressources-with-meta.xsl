<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:saxon="http://saxon.sf.net/" 
    extension-element-prefixes="saxon"
    >
    
      <!-- This XSL create an ANT script that will download our ressources (images) using the GET TASK-->
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <xsl:param name="dest-rep">${hdocResultPath}/ressources</xsl:param>        
        <project name="getRessourcesFiles" basedir="." default="getRessourcesFiles">
        <!-- Ccreation of the ressources folder -->            
        <mkdir dir="{$dest-rep}"/>
         
            <target name="getRessourcesFiles">
                <xsl:apply-templates select="//div[@class='thumb tleft' or @class='thumb tright']//img | //p//img | //center//img | //li//img"/>
                <!-- If an image doesn't appear in final result you should add it to the apply-template above-->
            </target>
        </project>
    </xsl:template>
    
    

    <!-- What has been changed starts from here  -->
    <xsl:template match="img" priority="5">
        <!-- Now the storage structure of the images will become :
        ressources
            image.jpeg (this is a folder)
                image.jpeg 
                meta.xml
        -->
        <!-- The meta.xml contains the copyright file of the image -->
        <!-- The meta.xml should respect the structure given to you in official-meta.xml (it's available in the same level as this file) )-->
        <xsl:param name="dest-rep-image">${hdocResultPath}/ressources/<xsl:value-of select="translate(tokenize(@src, '/')[last()],'?%','_')"/></xsl:param>
        <xsl:param name="dest-file">${hdocResultPath}/ressources/<xsl:value-of select="translate(tokenize(@src, '/')[last()],'?%','_')"/>/<xsl:value-of select="translate(tokenize(@src, '/')[last()],'?%','_')"/>
        </xsl:param>
        <xsl:param name="dest-file-meta">${hdocResultPath}/ressources/<xsl:value-of select="translate(tokenize(@src, '/')[last()],'?%','_')"/>/meta.xml</xsl:param>


        <xsl:param name="copyright-link"><xsl:value-of select="following-sibling::a[1]/@href"/></xsl:param>
         <mkdir dir="{$dest-rep-image}"/>
        <get src="{@src}" dest="{$dest-file}" /><!-- Get The image -->


        <!-- Get the copyright file of the image if it existt -->
        <xsl:if test="$copyright-link !=''" >
                <get src="{$copyright-link}" dest="{$dest-file-meta}" />
                <!-- Here You should generate Your XSL transformation to extract the right information from the copyright file (final result should be similar as official-meta.xml) 
                Very Important, The title of the image is not present in the copyright file BUT We have prepared it already in the @alt attribute
                -->
        </xsl:if>




    </xsl:template>
    

    
</xsl:stylesheet>