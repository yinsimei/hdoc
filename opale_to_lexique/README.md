# Converter opale_to_lexique

License GPL3.0
http://www.gnu.org/licenses/gpl-3.0.txt

##Crédits
Florian Arend, Antoine Aufrechter, Estelle de Magondeaux, Marouane Hammi 

##Install
In order to use this converter, follow those steps :
1. Copy your Opale file(s) (.scar or .zip) into the input directory.
2. Execute the run that correspond to your OS.
3. You will find the result into the output directory.The name of the output files depends on the hour it was processed.

##User documentation
Because the Opale definitions are much less structured that Lexique definitions, only the text content of the definition will be fill out in the Lexique definition. Be free to add other information or organize differently the resulting text.

