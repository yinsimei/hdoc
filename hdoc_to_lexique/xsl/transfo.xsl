<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    
	xmlns:hodoc="http://www.utc.fr/ics/hdoc/xhtml"

	exclude-result-prefixes="xs"
    version="2.0">

    <xsl:output method="xml" indent="yes" />
    
    <xsl:template match="/">
        <xsl:apply-templates select="./hodoc:html/hodoc:body"/>
    </xsl:template>

    
    
	<xsl:template match="hodoc:section">

		<xsl:result-document href="{hodoc:header/hodoc:h1}.term" encoding="UTF-8"> <!--TODO change name of the file-->
			<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
			xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:lx="scpf.org:lexicon"
			xmlns:file="java.io.File">
				<lx:term >
					<lx:termM>
						<sp:name><xsl:value-of select="hodoc:header/hodoc:h1"></xsl:value-of></sp:name>                    
					</lx:termM>
					<sp:def>
						<lx:definition>
							<sp:desc>
								<lx:defTxt>
									<sc:para xml:space="preserve" sc:id="t7"><xsl:apply-templates select="hodoc:div/hodoc:p[@span='def_content']/hodoc:p[@class='def_text']"></xsl:apply-templates></sc:para>
								</lx:defTxt>
							</sp:desc>
						</lx:definition>	
					</sp:def>
				</lx:term>
			</sc:item>
		</xsl:result-document>
	</xsl:template>
		
		    
    <xsl:template match="hodoc:p[@class='def_text']" >
        <xsl:value-of select="./text()"/>
		<!--
		<xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>-->
    </xsl:template>

</xsl:stylesheet>
