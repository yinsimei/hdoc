﻿Converter hdoc_to_neo4j
-----------------------

The purpose of this converter is to obtain a file of Neo4j requests from HDOC format. 


License GPL3.0
--------------

http://www.gnu.org/licenses/gpl-3.0.txt


Credits
-------

* Alié Félix

* Routier Clément


Dependance
----------


This project can be used alone if you only want to convert an HDOC into a Neo4j requests file.


User documentation
------------------

You have to respect the following steps :

0. If you start from an Opale format, you can use the opale_to_hdoc converter, or directly execute the opale_to_Neo4j one.

1. The first step is to put you HDOC document into the input directory inside the project hdoc_to_neo4j. Please, place only one file in that folder.

2. The second one is to launch the bash, the bat or start the ant (depending on your Operating System) by typing the following instruction : 
	
		ant -buildfile hdoc_to_neo4j

3. After the execution, you should find in the output folder (which is created in step 2) a result.cql file that contains all the requests to recreate the items and links of you hdoc input.


Unsupported 
-----------



Known bugs
----------



Todo
---- 



Technical notes
---------------



Capitalisation
--------------