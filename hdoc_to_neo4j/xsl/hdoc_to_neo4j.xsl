<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.utc.fr/ics/hdoc/xhtml"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="text" indent="no"/>
   
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="head"/>
    
    <xsl:template match="body">         
        <xsl:apply-templates select="./section"/>
    </xsl:template>
    
    <xsl:template match="body/section">
        MERGE (i:Item {title:"<xsl:value-of select="./header/h1"/>"});
        <xsl:apply-templates select="./section"/>
    </xsl:template>
    
    <xsl:template match="body/section/section">
        MERGE (i:Item {title:"<xsl:value-of select="./header/h1"/>"});
        MATCH (l1:Item {title:"<xsl:value-of select="./header/h1"/>"}), (l2:Item {title:"<xsl:value-of select="parent::section/header/h1"/>"})
        CREATE (l1)-[:LIEN]->(l2);
    </xsl:template>
</xsl:stylesheet>