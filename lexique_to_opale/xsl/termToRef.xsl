<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:lx="scpf.org:lexicon"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:op="utc.fr:ics/opale3"
    
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml" indent="yes" />
    
    <xsl:template  match="/">
        <sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
            <op:glos xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:op="utc.fr:ics/opale3">
                <op:glosM>
                    <sp:term>
                        <xsl:value-of select="sc:item/lx:term/lx:termM/sp:name"></xsl:value-of>
                        <xsl:apply-templates select="sc:item/lx:term/lx:termM/sp:grammar"></xsl:apply-templates>
                    </sp:term>
                    <sp:def>
                        <op:sTxt>
                            <xsl:apply-templates select="/sc:item/lx:term"></xsl:apply-templates>
                        </op:sTxt>
                    </sp:def>
                </op:glosM>
            </op:glos>
        </sc:item>
    </xsl:template>
    
    <!--
        RECUPERATION DU TERME
        -->
    
    <xsl:template match="lx:termM/sp:grammar">
        (<xsl:choose>
            <xsl:when test="text() = 'female'">nf.</xsl:when>
            <xsl:when test="text() = 'male'">nm.</xsl:when>
            <xsl:when test="text() = 'adj'">adj.</xsl:when>
            <xsl:when test="text() = 'verb'">verb.</xsl:when>
        </xsl:choose>)
    </xsl:template>
    
    <xsl:template match="lx:term">
        <!-- Definition(s) -->
        <xsl:choose>
            <xsl:when test="count(sp:def) > 1">
                <sc:orderedList>
                    <xsl:apply-templates select="sp:def/lx:definition" mode="inList"></xsl:apply-templates>
                </sc:orderedList>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="sp:def/lx:definition"></xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
        <!-- Tags -->
        <xsl:apply-templates select="/sc:item/lx:term/sp:index/lx:index"></xsl:apply-templates>
    </xsl:template>
    
    <!--
        GESTION DES DEFINITIONS
        -->
    
    <xsl:template match="lx:term/sp:def/lx:definition" mode="inList">
        <sc:listItem>
            <xsl:apply-templates select="sp:desc/lx:defTxt/*"></xsl:apply-templates>
            <xsl:apply-templates select="sp:source"></xsl:apply-templates>
            <xsl:apply-templates select="sp:moreInfo"></xsl:apply-templates>
        </sc:listItem>        
    </xsl:template>
    
    <xsl:template match="lx:term/sp:def/lx:definition">
        <xsl:apply-templates select="sp:desc/lx:defTxt/*"></xsl:apply-templates>
        <xsl:apply-templates select="sp:source"></xsl:apply-templates> 
        <xsl:apply-templates select="sp:moreInfo"></xsl:apply-templates>
    </xsl:template>
    
    <!-- Information Complémentaire -->
    <xsl:template match="sp:moreInfo">
        <xsl:apply-templates select="lx:moreInfoM/sp:title"></xsl:apply-templates>
        <sc:itemizedList>
            <sc:listItem>
                <xsl:apply-templates select="lx:txt/*"></xsl:apply-templates>
            </sc:listItem>
        </sc:itemizedList>
    </xsl:template>
    
    <xsl:template match="lx:moreInfoM/sp:title">
        <sc:para><sc:inlineStyle role="emp"><xsl:value-of select="text()"></xsl:value-of> :</sc:inlineStyle></sc:para>
    </xsl:template>
    
    <!-- Copie du contenu de la balise lx:defTxt et moreInfo -->
    <xsl:template match="lx:defTxt//*|lx:defTxt//@*|sp:moreInfo/lx:txt//*|sp:moreInfo/lx:txt//@*" >
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

	<xsl:template match="lx:defTxt/lx_defTxt_blocs/sc:para" >
        <i>definition : </i>
	<xsl:value-of select="."/>
    </xsl:template>
    
    <!--
        FILTRAGE DES CONTENUS DE LA DEFINITION
        -->
    
    <!-- Ne seront pas pris en compte illustrations video et tableaux -->
    <xsl:template match="sc:extBlock|sc:table" priority="2" >
        <!-- a revoir 
        <xsl:if test="not(following-sibling::node()) and not(preceding-sibling::node())">
            <sc:para></sc:para>
        </xsl:if>-->
    </xsl:template>
    
    <!-- Les liens vers d'autres termes,les imagettes ne sont pas reprise mais leur contenu oui -->
    <xsl:template match="sc:inlineImg|sc:uLink" priority="2">
        <xsl:apply-templates select="*|text()"></xsl:apply-templates>
    </xsl:template>
    
    <!-- reprend le contenu des citations Lexique et les met entre guillemets -->
    <xsl:template match="sc:phrase[@role='quote']" priority="2">
        &#171;&#160;<xsl:apply-templates select="*|text()"></xsl:apply-templates>&#160;&#187;
    </xsl:template>
    
    <!-- transforme les termes spécifiques de Lexique en termes étrangers de Opale -->
    <xsl:template match="sc:phrase[@role='special']" priority="2">
        <sc:inlineStyle role="spec"><xsl:apply-templates select="*|text()"></xsl:apply-templates></sc:inlineStyle>
    </xsl:template>
    
    <!-- remplace 'emphasis' par 'emp' -->
    <xsl:template match="@role[.='emphasis']" priority="2">
        <xsl:attribute name="role">emp</xsl:attribute>
    </xsl:template>
    
    <!-- Gestion des URLs -->
    <xsl:template match="lx:urlM" priority="2">
        <op:urlM>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </op:urlM>
    </xsl:template>
    
    <!--
        GESTION DES SOURCES
        -->
    
    <xsl:template match="lx:definition/sp:source[@sc:refUri]">
        <!--<xsl:variable name="path" select="resolve-uri(replace(@sc:refUri,'^/',''),concat('file:/',replace($srcdir,'\\','/')))"></xsl:variable>-->
        <xsl:apply-templates select="document(@sc:refUri)/sc:item/lx:source/lx:sourceM"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="lx:definition/sp:source[not(@sc:refUri)]">
        <xsl:apply-templates select="lx:source/lx:sourceM"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="lx:source/lx:sourceM[sp:url]">
        <sc:para>
            <sc:inlineStyle role="spec">
                <xsl:text>Source : </xsl:text>
                <sc:phrase role="url">
                   <op:urlM>
                       <sp:url>
                           <xsl:value-of select="sp:url/text()"></xsl:value-of>
                       </sp:url>
                   </op:urlM>
                   <xsl:value-of select="sp:title"></xsl:value-of>
                </sc:phrase>
            </sc:inlineStyle>
        </sc:para>
    </xsl:template>
    
    <xsl:template match="lx:source/lx:sourceM[not(sp:url)]">
        <sc:para>
            <sc:inlineStyle role="spec">
                <xsl:text>Source : </xsl:text>
                <xsl:value-of select="sp:title"></xsl:value-of>
            </sc:inlineStyle>
        </sc:para>
    </xsl:template>
    
    <!--
        GESTION DES TAGS
        -->
    <xsl:template match="sp:index/lx:index">
        <sc:para>
            <xsl:text>Concerne : </xsl:text>
            <xsl:apply-templates select="sp:tag"></xsl:apply-templates>
        </sc:para>
    </xsl:template>
    
    <xsl:template match="lx:index/sp:tag">
            <!--<xsl:variable name="path" select="resolve-uri(replace(@sc:refUri,'^/',''),concat('file:/',replace($srcdir,'\\','/')))"></xsl:variable>-->
            <xsl:if test="position() != 1">
                <xsl:text>, </xsl:text>
            </xsl:if>
        <xsl:value-of select="document(@sc:refUri)/sc:item/lx:tag/lx:tagM/sp:title"></xsl:value-of>
    </xsl:template>
    
</xsl:stylesheet>
